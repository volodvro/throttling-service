package throttling

import akka.actor.ActorSystem

import scala.concurrent.{Future, Await}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
 * Created by volodvro on 19.08.16.
 */
object Tester extends App {

  val system = ActorSystem("throttling")

  val service: ThrottlingService = new RequestThrottlingService(10, system)
  Await.result(Future.sequence((1 to 10).map { _ =>
    Seq(
      Future {
        service.isRequestAllowed(Some("Vova"))
      },
      Future {
        service.isRequestAllowed(Some("SomeGuy"))
      }
    )
  } flatten), 1 seconds).toSeq.foreach(println(_))
  system.terminate()

}
