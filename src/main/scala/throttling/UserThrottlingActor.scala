package throttling

import akka.actor.{Actor, ActorLogging}
import throttling.UserThrottlingActor.PermitRequest

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
 * Created by volodvro on 18.08.16.
 */
class UserThrottlingActor(sla:Sla) extends Actor with ActorLogging {

  private val schedule = context.system.scheduler.schedule(0 seconds, 100 millis, self, PermitRequest)
  var freeRequests:Int = sla.maxRps

  override def receive: Receive = {
    case PermitRequest =>
      if (freeRequests < sla.maxRps ) freeRequests += (sla.maxRps * 0.1).toInt
    case Sla(name, rps) =>
      if (freeRequests == 0) {
        sender() ! false
      } else {
        freeRequests -= 1
        sender() ! true
      }
    case _ => println("recieved smth")
  }

  @throws[Exception](classOf[Exception])
  override def postStop(): Unit = {
    schedule.cancel()
    super.postStop()
  }
}

object UserThrottlingActor {
  def apply(sla:Sla) = new UserThrottlingActor(sla)

  case object PermitRequest
}
