package throttling

import scala.concurrent.Future

/**
 * Created by volodvro on 18.08.16.
 */
case class Sla(userName:String, maxRps:Int)

trait SLAService {
  def getSlaByToken(token:String):Future[Sla]
}



