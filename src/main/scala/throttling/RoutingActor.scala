package throttling

import java.util.concurrent.{ConcurrentHashMap, ConcurrentMap}

import akka.actor.{Actor, ActorRef, Props}
import akka.util.Timeout

import scala.collection.JavaConversions._
import scala.concurrent.duration._

/**
 * Created by volodvro on 19.08.16.
 */
class RoutingActor extends Actor {
  val userActors: ConcurrentMap[String, ActorRef] = new ConcurrentHashMap[String, ActorRef]

  override def receive: Receive = {

    case sla @ Sla(name, _) =>
      implicit val timeout = Timeout(1 seconds)

      userActors.getOrElseUpdate(sla.userName, {
        context.system.actorOf(Props(UserThrottlingActor(sla)), name)
      })
      userActors(name) forward sla

  }
}
