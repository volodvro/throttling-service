package throttling

import akka.actor.{ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
 * Created by volodvro on 19.08.16.
 */

class RequestThrottlingService(override val GraceRps:Int = 10, actorSystem: ActorSystem) extends ThrottlingService {

  val duration = 1 seconds
  val defaultSla = Sla("undefined", GraceRps)
  override val slaService = new MockSlaService

  override def isRequestAllowed(token:Option[String]):Boolean = {
    Await.result(token match {
      case Some(value) =>
        slaService.getSlaByToken(value).
          recover({
            case e => defaultSla
          }).
          flatMap {sendRequest}.
          mapTo[Boolean]

      case None => sendRequest(defaultSla).
        mapTo[Boolean].
        recover({
          case ex:Exception => false
        })
    }, duration)
  }

  implicit val timeout = Timeout(1 seconds)

  val routingActor = actorSystem.actorOf(Props[RoutingActor])

  def sendRequest(sla:Sla) = (routingActor ? sla).mapTo[Boolean]

}

