package throttling

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
/**
 * Created by volodvro on 19.08.16.
 */
class MockSlaService extends SLAService {
  override def getSlaByToken(token: String): Future[Sla] = Future {
    token match {
      case "Vova" => Sla("Vova", 10)
      case "Dima" => Sla("Dima", 10)
    }
  }
}
