package throttling

trait ThrottlingService {

  val GraceRps:Int // configurable

  val slaService: SLAService // use mocks for testing
  // Should return true if the request is within allowed RPS.
  def isRequestAllowed(token:Option[String]):Boolean
}