package throttling

import java.util.concurrent.{ConcurrentHashMap, ConcurrentMap}

import scala.collection.JavaConversions._
import scala.concurrent.Future

/**
 * Created by volodvro on 18.08.16.
 */
trait CachedSlaService extends SLAService {

  val cache: ConcurrentMap[String, Future[Sla]] = new ConcurrentHashMap[String, Future[Sla]]

  abstract override def getSlaByToken(token:String):Future[Sla] = {
    cache.getOrElseUpdate(token, {
      super.getSlaByToken(token)
    })
    cache(token)
  }
}
