package rest

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import spray.can.Http
import spray.http._


import spray.http.HttpRequest
import spray.routing.SimpleRoutingApp

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import spray.client.pipelining._
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by volodvro on 22.08.16.
 */
object PerformanceTester extends App with SimpleRoutingApp {
  implicit val system = ActorSystem("throttling")
  val service = system.actorOf(Props[ThrottlingRestService], "restServiceActor")

  implicit val timeout = Timeout(30.seconds)
  IO(Http) ? Http.Bind(service, interface = "localhost", port = 8080)

  val pipeline: HttpRequest => Future[HttpResponse] = addHeader("Authorization", "Someone") ~> sendReceive

  val timeWithThrottling = countRequiredTime("http://localhost:8080/request", 100)
  val timeSimpleRequest = countRequiredTime("http://localhost:8080/simple", 100)
  val timesFaster = timeWithThrottling.toDouble / timeSimpleRequest.toDouble

  println(s"Simple request works in ${timesFaster} times faster")

  def countRequiredTime(path:String, repeat: Int) = {
    val startTime = System.currentTimeMillis()

    for (i <- 0 to repeat) {
      val requestFutures = for (i <- 1 to 10000) yield pipeline(Get(path))
      Await.result(Future.sequence(requestFutures), 30 seconds)
    }

    val endTime = System.currentTimeMillis()
    endTime - startTime
  }

  //  For 100 repetitions with 10k requests each Simple request works in 1.9027241585558154 times faster
}
