package rest

import akka.actor.Actor
import spray.http.HttpResponse
import spray.routing.HttpService
import spray.routing.RejectionHandler.Default
import throttling.RequestThrottlingService

/**
 * Created by volodvro on 22.08.16.
 */
class ThrottlingRestService extends Actor with HttpService {
  val throttlingService = new RequestThrottlingService(100000, context.system)

  override def receive = runRoute(throttling ~ simple)
  val throttling = {
    path("request") {
      get {
        optionalHeaderValueByName("Authorization") { token =>
          complete {
            throttlingService.isRequestAllowed(token)
            HttpResponse(200)
          }
        }
      }
    }
  }

  val simple = {
    path("simple") {
      get {
          complete {
            HttpResponse(200)
          }
      }
    }
  }

  override implicit def actorRefFactory = context
}
