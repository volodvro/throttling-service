package throttling

/**
 * Created by volodvro on 19.08.16.
 */

import akka.actor.ActorSystem
import akka.testkit.TestKit
import org.scalatest._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class ThrottlingServiceTest extends TestKit(ActorSystem("throttling")) with WordSpecLike with Matchers {

  val service: ThrottlingService = new RequestThrottlingService(10, system)
  "All requests from registered user" should {"be successfully processed" in {
      for (i <- 1 to 10) {
        Await.result(Future.sequence((1 to 10).map { _ =>
          Future {
            service.isRequestAllowed(Some("Vova"))
          }
        }), 1 seconds).toIndexedSeq.foreach(_ should equal(true))
        Thread.sleep(1000)
      }
    }
  }

  "requests after 10" should {"be rejected" in {
        assert(Await.result(Future.sequence((1 to 12).map { _ =>
          Future {
            service.isRequestAllowed(Some("Vova"))
          }
        }), 1 seconds).seq.count(_ == false) == 2)
    }
  }

  "requests of both users" should {"be successful" in {
    Thread.sleep(1000)
        assert(Await.result(Future.sequence((1 to 10).map { _ =>
          Seq(
            Future {
              service.isRequestAllowed(Some("Vova"))
            },
            Future {
              service.isRequestAllowed(Some("SomeGuy"))
            }
          )
        } flatten), 1 seconds).toSeq.forall(_ == true))
    }
  }
}
