import sbt._

object Dependencies {
  lazy val `akka-actor` = "com.typesafe.akka" % "akka-actor_2.11" % "2.4.8"
  lazy val `spray-json` = "io.spray" %%  "spray-json" % "1.3.2"

  lazy val `scalactic` = "org.scalactic" %% "scalactic" % "3.0.0"
  lazy val `scala-test` = "org.scalatest" %% "scalatest" % "3.0.0" % "test"
  lazy val `scala-mock` = "org.scalamock" %% "scalamock-scalatest-support" % "3.2.2" % "test"
  lazy val `akka-test-kit` = "com.typesafe.akka" %% "akka-testkit" % "2.3.9"
  lazy val `spray-can` = "io.spray" %% "spray-can" % "1.3.2"
  lazy val `spray-routing` = "io.spray" %% "spray-routing" % "1.3.2"
  lazy val `spray-client` = "io.spray" %% "spray-client" % "1.3.2"


  lazy val dependencies = Seq(`akka-actor`, `spray-json`, `scala-test`, `scala-mock`, `akka-test-kit`, `scalactic`,
  `spray-can`, `spray-routing`, `spray-client`)
}

