import sbt.Keys._

lazy val commonSettings = Seq(
  version := "1.0",
  scalaVersion := "2.11.8",
  libraryDependencies ++= Dependencies.dependencies,
  resolvers += "spray repo" at "http://repo.spray.io"
)

lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  settings(name := "root")
